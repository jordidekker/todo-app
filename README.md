Todo-App
========================

Setup using Docker
------------------

1 -  Build the production image in the /docker/ directory
```
docker build -t todo-app .
```

2 - Build the development image in the /docker/ directory
```
docker build -t todo-app-dev -f Dockerfile_dev .
```

3 -  Run the stack in the /docker/ directory
```
docker-compose up
```

4 - Get container id of todo-app
```
docker ps
```

4 -  Create the database with schema
```
docker exec -t -i [id] bash
bin/console doctrine:database:create
bin/console doctrine:schema:create
```

Development
-----------------

Inside the development image you can use NPM, Yarn & PHPUnit.

Run tests inside the todo-app-dev image
```
docker exec -t -i [id] bash
vendor/bin/phpunit
```

Visit the todo-app
-----------------

Production:
```
http://localhost:8080/
```

Development:
```
http://localhost:8081/
```