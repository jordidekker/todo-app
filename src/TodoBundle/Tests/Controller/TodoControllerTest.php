<?php

namespace TodoBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;

class TodoControllerTest extends WebTestCase
{
    public function testCompleteCrudScenario()
    {
        // Create a new client
        $client = static ::createClient();

        // Create a new entry in the database
        $crawler = $client->request('GET', '/todo/new');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        // Fill in the form and submit it
        $form = $crawler->selectButton('Opslaan')->form(array(
            'todobundle_todo[title]' => 'My test ToDo',
            'todobundle_todo[deadline][day]'   => '27',
            'todobundle_todo[deadline][month]' => '9',
            'todobundle_todo[deadline][year]'  => '2017',
        ));
        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check redirect to showAction and data in the view
        $this->assertEquals('TodoBundle\Controller\TodoController::showAction', $client->getRequest()->attributes->get('_controller'));
        $this->assertGreaterThan(0, $crawler->filter('td:contains("My test ToDo")')->count());

        // Edit the entity
        $crawler = $client->click($crawler->selectLink('Bewerken')->link());

        // Check if page is from editAction
        $this->assertEquals('TodoBundle\Controller\TodoController::editAction', $client->getRequest()->attributes->get('_controller'));

        // Save form with new title
        $form = $crawler->selectButton('Opslaan')->form(array(
            'todobundle_todo[title]'  => 'Edited ToDo',
        ));
        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check if redirected to editAction after submitting form
        $this->assertEquals('TodoBundle\Controller\TodoController::editAction', $client->getRequest()->attributes->get('_controller'));

        // Check the element contains an attribute with value equals "Edited ToDo"
        $this->assertGreaterThan(0, $crawler->filter('[value="Edited ToDo"]')->count());

        // Delete the entity
        $client->submit($crawler->selectButton('Verwijderen')->form());
        $crawler = $client->followRedirect();

        // Check if redirected to indexAction after deleting entry
        $this->assertEquals('TodoBundle\Controller\TodoController::indexAction', $client->getRequest()->attributes->get('_controller'));

        // Check the entity has been delete on the list
        $this->assertNotRegExp('/My test ToDo/', $client->getResponse()->getContent());
    }

    public function testIndexActionRoute()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('TodoBundle\Controller\TodoController::indexAction', $client->getRequest()->attributes->get('_controller'));
    }

    public function testCreateActionWithoutTitle()
    {
        // Create a new client
        $client = static ::createClient();

        // Create a new entry in the database
        $crawler = $client->request('GET', '/todo/new');
        // Fill in the form and submit it
        $form = $crawler->selectButton('Opslaan')->form(array(
            'todobundle_todo[title]' => '',
        ));
        $client->submit($form);

        $crawler = new Crawler($client->getResponse()->getContent());
        $this->assertGreaterThanOrEqual(1, $crawler->filter('span.help.is-danger')->count());
    }
}
