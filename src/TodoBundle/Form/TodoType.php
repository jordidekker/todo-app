<?php

namespace TodoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TodoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'attr'     => [
                    'class' => 'field'
                ],
                'label'    => 'Titel',
                'required' => true
            ])
            ->add('deadline', DateType::class, [
                'attr'        => [
                    'class' => 'field'
                ],
                'format'      => 'd M yyyy',
                'label'       => 'Deadline (optioneel)',
                'placeholder' => [
                    'year'  => 'Jaar',
                    'month' => 'Maand',
                    'day'   => 'Dag'
                ],
                'required'    => false
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TodoBundle\Entity\Todo'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'todobundle_todo';
    }


}
